#ifndef GLOBALS_H
#define GLOBALS_H
#include <iostream>
#include <chrono>

#define downloadURL "https://www.nasdaq.com/screening/companies-by-name.aspx?pagesize=200&page=35&render=download"
#define downloadURLJSON "https://api.iextrading.com/1.0/ref-data/symbols"
#define downloadNameJSON "../symbols.json"
#define NUM_THREADS 32
#define downloadName "../companylist.csv"


/*
namespace GlobalVariables {
	//std::string downloadURL = "https://www.nasdaq.com/screening/companies-by-name.aspx?pagesize=200&page=35&render=download";
	//std::string downloadName = "../companylist.csv";


	/*
	int second = 1;
	int minute = 60 * second;
	int hour = 60 * minute;
	int day = 24 * hour;
	int OPEN = ((14 * hour) + (25 * minute));
	int CLOSE = ((21 * hour) + (05 * minute));
	//opens 9:30am to 4:00pm eastern time
	//UTC time is +5 hours ahead of eastern time
	
}
*/
#endif