#include "Stock.h"



Stock::Stock(){

}

Stock::Stock(CompanyDataIEX company) {
	data = company;
}

Stock::~Stock()
{
}

void Stock::update(double _current, double _open, double _high, double _low,
			double _yearHigh, double _yearLow, std::string _date) {
	open.push_back(_open);
	high.push_back(_high);
	low.push_back(_low);
	yearHigh = yearHigh;
	yearLow = yearLow;
	date.push_back(_date);
	current.push_back(_current);
}

CompanyDataIEX Stock::getData() {
	return data;
}

std::vector<double> &Stock::getCurrent() {
	return current;
}

void Stock::WriteDataToFile(double _current, double _open, double _high, double _low,
	double _yearHigh, double _yearLow, std::string _time, std::string _date) {

	//C:/Users/jmhug/source/repos/Stock
	std::string path = "../Data/";
	std::string file = path + data.symbol + ".csv";

	std::ofstream ofile;

	ofile.open(file.c_str(), std::ios_base::app);
	if (ofile.good()) {
		std::string line = "\"" + data.symbol + "\",\"" + _time + "\",\"" + convertDouble(_current) +
			"\",\"" + convertDouble(_open) + "\",\"" + convertDouble(_high)
			+ "\",\"" + convertDouble(_low) + "\",\"" + convertDouble(_yearHigh)
			+ "\",\"" + convertDouble(_yearLow) + "\",\n";
		ofile << line;
		ofile.flush();
		ofile.close();
	}


}

std::string Stock::convertDouble(double x) {
	std::ostringstream strs;
	strs << x;
	return strs.str();
}