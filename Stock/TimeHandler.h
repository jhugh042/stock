#ifndef TIMEHANDLER_H
#define TIMEHANDLER_H
#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <iostream>
class TimeHandler
{
public:
	TimeHandler();
	~TimeHandler();
	long getSeconds();
	long getMilliseconds();
	long getSecondsOfDay();
	int difference(TimeHandler);
	std::chrono::high_resolution_clock::time_point getTimeObject();
	void updateTime();

	bool openMarket();

	int sleepTime();
	std::string getDate();

private:
	std::chrono::high_resolution_clock::time_point time;
	std::chrono::system_clock::time_point system;

	int second = 1;
	int minute = 60 * second;
	int hour = 60 * minute;
	int day = 24 * hour;
	int OPEN = ((14 * hour) + (25 * minute));//
	int CLOSE = ((21 * hour) + (05 * minute));
	//opens 9:30am to 4:00pm eastern time
	//chrono time is +5 hours ahead
protected:

};

#endif