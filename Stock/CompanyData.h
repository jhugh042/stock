#ifndef COMPANYDATA_H
#define COMPANYDATA_H

#include <string>

//https://api.iextrading.com/1.0/ref-data/symbols
struct CompanyDataIEX {

	CompanyDataIEX() {
		symbol = "";
		url = "";
		name = "";
		date = "";
		enabled = false;
		type = "";
		id = -1;
	}

	CompanyDataIEX(std::string _symbol, std::string _name, std::string _date,
		bool _enabled, std::string _type, double _id, std::string _url) {
		symbol = _symbol;
		url = _url;
		name = _name;
		date = _date;
		enabled = _enabled;
		type = _type;
		id = _id;
	}

	std::string symbol;
	std::string url;
	std::string name;
	std::string date;
	bool enabled;
	std::string type;
	double id;
};

#endif