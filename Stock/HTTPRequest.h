#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H
#include <iostream>
#include <stdio.h>
#include <curl\curl.h>
#include <string.h>
#include <vector>
#include <thread>
#include "Globals.h"
#include "Stock.h"
#include "json.hpp"
#include "TimeHandler.h"

struct FtpFile {
	const char *filename;
	FILE *stream;
};

class HTTPRequest{
public:
	HTTPRequest();
	~HTTPRequest();

	static bool Download(std::string, std::string);
	static void PopulateData(std::vector<Stock> &companies);
	static void PullHTML(std::vector<Stock> &companies, int start, int end, int thread);
private:
	static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);
	static size_t FileWrite(void *buffer, size_t size, size_t nmemb, void *stream);

	static void ParseJSON(Stock&, std::string);
protected:

};

#endif