#include "HTTPRequest.h"



HTTPRequest::HTTPRequest(){

}


HTTPRequest::~HTTPRequest(){

}

void HTTPRequest::PopulateData(std::vector<Stock> &companies) {

	std::thread threads[NUM_THREADS];
	curl_global_init(CURL_GLOBAL_ALL);

	const int size = companies.size() / NUM_THREADS;

	std::vector<Stock> vectors[NUM_THREADS];

	for (int i = 0; i < NUM_THREADS; i++) {

		int start = i * size;
		int end = ((i*size) + (size - 1));

		if (i == (NUM_THREADS - 1)) {
			end = companies.size();
			//std::cout << "Size: " << end << "\n";
		}
		threads[i] = std::thread(HTTPRequest::PullHTML, std::ref(companies), start, end, i);
	}
	for (int i = 0; i < NUM_THREADS; i++) {
		threads[i].join();
	}
}

void HTTPRequest::PullHTML(std::vector<Stock> &companies, int start, int end, int thread) {

	for (int i = start; i < end; i++) {

		Stock it = companies.at(i);

		CURL *curl;
		CURLcode res;
		std::string readBuffer;

		curl = curl_easy_init();
		if (curl) {
			curl_easy_setopt(curl, CURLOPT_URL, it.getData().url.c_str());
			curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
			curl_easy_setopt(curl, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
			//curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
			res = curl_easy_perform(curl);
			curl_easy_cleanup(curl);

			if (CURLE_OK != res) {
				printf("Error pulling data... (Sleeping for 1 second)\n");
				Sleep(1000);
			}
			else {
				
				std::size_t not_found = readBuffer.find("Not Found");
				if (not_found != std::string::npos) {
					//error with the page
					printf("Error finding the data for symbol: %s", it.getData().symbol.c_str());
				}
				else {
					//std::cout << "URL: " << it.getData().url << "\n" << readBuffer;
					ParseJSON(it, readBuffer);
				}
				companies[i] = it;
				//std::cout << "Symbol: " << it.getData().symbol << "\t\tThread: " << thread << "\n";
			}
		}
	}
}

bool HTTPRequest::Download(std::string url, std::string fileName) {

	CURL *curl;
	CURLcode res;
	struct FtpFile ftpfile = {
		fileName.c_str(), /* name to store the file as if successful */
		NULL
	};

	curl = curl_easy_init();
	if (curl) {
		/*
		* You better replace the URL with one that works!
		*/
		curl_easy_setopt(curl, CURLOPT_URL,url.c_str());
		/* Define our callback to get called when there's data to be written */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, FileWrite);
		/* Set a pointer to our struct to pass to the callback */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);

		/* Switch on full protocol/debug output */
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

		res = curl_easy_perform(curl);

		/* always cleanup */
		curl_easy_cleanup(curl);

		if (CURLE_OK != res) {
			/* we failed */
			fprintf(stderr, "curl told us %d\n", res);
			return false;
		}
	}

	if (ftpfile.stream)
		fclose(ftpfile.stream); /* close the local file */

	curl_global_cleanup();

	return true;
}

size_t HTTPRequest::FileWrite(void *buffer, size_t size, size_t nmemb, void *stream)
{
	struct FtpFile *out = (struct FtpFile *)stream;
	if (out && !out->stream) {
		/* open file for writing */
		fopen_s(&out->stream, out->filename, "wb");
		if (!out->stream)
			return -1; /* failure, can't open file to write */
	}
	return fwrite(buffer, size, nmemb, out->stream);
}

size_t HTTPRequest::WriteCallback(void *buffer, size_t size, size_t nmemb, void *stream)
{
	((std::string*)stream)->append((char*)buffer, size * nmemb);
	return size * nmemb;
}


void HTTPRequest::ParseJSON(Stock &data, std::string readBuffer) {

	double open = -1;
	double high = -1;
	double low = -1;
	double current = -1;
	double yearLow = -1;
	double yearHigh = -1;
	std::string time = "N/A";


	nlohmann::json json;
	try {
		json = *nlohmann::json::parse(readBuffer).begin();
	}
	catch (nlohmann::json::parse_error &e)
	{
		std::cout << "Symbol: " << data.getData().symbol << "\n";
		std::cout << e.what() << std::endl;
		std::cout << readBuffer << "\n";
		std::cin.get();
	}
	for (nlohmann::json::iterator it = json.begin(); it != json.end(); ++it) {
		//std::cout << it.key() << " : " << it.value() << '\n';

		if (it.key() == "latestPrice") {
			if (it.value().is_number()) {
				current = it.value().get<double>();
			}
		}
		else if (it.key() == "open") {
			if (it.value().is_number()) {
				open = it.value().get<double>();
			}
		}
		else if (it.key() == "week52High") {
			if (it.value().is_number()) {
				yearHigh = it.value().get<double>();
			}
		}
		else if (it.key() == "week52Low") {
			if (it.value().is_number()) {
				yearLow = it.value().get<double>();
			}
		}
		else if (it.key() == "high") {
			if (it.value().is_number()) {
				high = it.value().get<double>();
			}
		}
		else if (it.key() == "low") {
			if (it.value().is_number()) {
				low = it.value().get<double>();
			}
		}
		else if (it.key() == "latestTime") {
			if (it.value().is_string()) {
				time = it.value().get<std::string>();
			}
		}
	}
	//data.update(current, open, high, low, yearHigh, yearLow, time);
	data.WriteDataToFile(current, open, high, low, yearHigh, yearLow, time, TimeHandler().getDate());

}