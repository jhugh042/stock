#include "HTTPRequest.h"
#include "TimeHandler.h"
#include <experimental\filesystem>
#include <fstream>

#ifdef _DEBUG
#define OVERRIDE false
#else
#define OVERRIDE false
#endif

void createFolder(std::string path) {
	if (!std::experimental::filesystem::exists(path.c_str())) {
		//Data folder does not exist, create it
		std::experimental::filesystem::create_directory(path.c_str());
	}
}

void createFile(std::string file) {
	std::ofstream ofile;
	if (!std::experimental::filesystem::exists(file.c_str())) {
		//file does not exist
		ofile.open(file.c_str(), std::ios_base::app);
		if (ofile.good()) {
			std::string line = "\"Symbol\",\"time\",\"latestPrice\",\"open\",\"high\",\"low\",\"yearHigh\",\"yearLow\",\n";
			ofile << line;
			ofile.flush();
			ofile.close();
		}
	}
}


bool ReadSymbols(std::vector<Stock> &companies) {

	std::ifstream file(downloadNameJSON);
	if (file.good()) {
		nlohmann::json jsonFile = nlohmann::json::parse(file);
		//std::cout << j.dump() << "\n";

		for (nlohmann::json::iterator it = jsonFile.begin(); it != jsonFile.end(); ++it) {
			//std::cout << *it << '\n';
			nlohmann::json line = *it;


			CompanyDataIEX data;

			for (nlohmann::json::iterator it2 = line.begin(); it2 != line.end(); ++it2) {

				//std::cout << it2.key()<<" : "<<it2.value() << '\n';
				if (it2.key() == "date") {
					data.date = it2.value().get<std::string>();
				}
				else if (it2.key() == "iexId") {
					//no need for this
				}
				else if (it2.key() == "symbol") {
					data.symbol = it2.value().get<std::string>();
				}
				else if (it2.key() == "name") {
					data.name = it2.value().get<std::string>();
				}
				else if (it2.key() == "type") {
					data.type = it2.value().get<std::string>();
				}
				else if (it2.key() == "isEnabled") {
					if (it2.value().is_boolean()) {
						data.enabled = it2.value().get<bool>();
					}
				}
				else {
					std::cout << "Error reading value: " << *it2 << "\n";
				}
			}
			data.url = "https://api.iextrading.com/1.0/stock/" + data.symbol + "/batch?types=quote";
			std::size_t found = data.symbol.find("#");

			if (found == std::string::npos) {
				found = data.symbol.find("*");
				if (found == std::string::npos) {
					std::string path = "../Data/";
					std::string file = path + data.symbol + ".csv";
					createFolder(path);
					createFile(file);
					companies.push_back(Stock(data));
				}
			}
		}
	}
	else {
		return false;
	}
	return true;
}

void test() {

	std::cout << "TIME: "<<TimeHandler().getSecondsOfDay()<<" Sleep for:" << TimeHandler().sleepTime() << "\n";
	std::cout << "TIME SEC: " << TimeHandler().getSeconds() << "\n";
	std::cout << "Date: " << TimeHandler().getDate() << "\n";
}

int main(void)
{
	test();
	TimeHandler time;
	//download excel from this link:
	//https://www.nasdaq.com/screening/companies-by-name.aspx?pagesize=200&page=35&render=download


	while (true) {
		time.updateTime();
		if (time.openMarket() || OVERRIDE) {
			std::vector<Stock> companies;

			//download cvs file and time how long it takes
			TimeHandler start;

			bool result = HTTPRequest::Download(downloadURLJSON, downloadNameJSON);
			std::cout << "Finished downloading Symbols list... (" << start.difference(TimeHandler()) << " milliseconds)\n";

			if (result) {

				start.updateTime();

				if (ReadSymbols(companies)) {
					std::cout << "Finished reading Symbols list... (" << start.difference(TimeHandler()) << " milliseconds, " << companies.size() << " entries)\n";

					while (time.openMarket() || OVERRIDE) {
						start.updateTime();
						HTTPRequest::PopulateData(companies);
						std::cout << "Finished one HTTPRequest loop... (" << start.difference(TimeHandler()) << " milliseconds)\n";
						time.updateTime();
					}
				}
				else {
					printf("Error reading file\n");
				}
			}
			else {
				printf("Error downloading file... (Sleeping for 10 seconds)\n");
				Sleep(10000);
			}
		}
		else {
			int sleeptime = time.sleepTime();
			std::cout << "Market Closed, Sleeping for: "<< sleeptime<<" seconds.\n";
			Sleep(sleeptime *1000);
		}
	}
	return 0;
}