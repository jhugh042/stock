#ifndef STOCK_H
#define STOCK_H
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
//#include <filesystem>
#include "CompanyData.h"

/*
#ifdef _WIN32
#include <windows.h>
#include <stdio.h>
#endif

#ifdef linux
#include <unistd.h>
#include <stdlib.h>
#endif
*/

class Stock
{
public:
	Stock();
	Stock(CompanyDataIEX);
	~Stock();

	void update(double current,double _open, double _high, double _low,
		double _yearHigh, double _yearLow, std::string _date);

	CompanyDataIEX getData();
	std::vector<double> &getCurrent();

	void WriteDataToFile(double _current, double _open, double _high, double _low,
		double _yearHigh, double _yearLow,std::string _time, std::string _date);
private:
	CompanyDataIEX data;

	std::vector<double> open;
	std::vector<double> current;
	std::vector<double> high;
	std::vector<double> low;
	double yearHigh;
	double yearLow;
	std::vector<std::string> date;

	std::string convertDouble(double);
};
#endif
