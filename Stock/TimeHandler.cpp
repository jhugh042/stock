#include "TimeHandler.h"



TimeHandler::TimeHandler(){
	updateTime();
}

TimeHandler::~TimeHandler(){

}

long TimeHandler::getSeconds() {

	return std::chrono::duration_cast
		<std::chrono::seconds>
		(system.time_since_epoch()).count();
}

long TimeHandler::getMilliseconds() {

	return std::chrono::duration_cast
		<std::chrono::milliseconds>
		(system.time_since_epoch()).count();
}

//used for precision
int TimeHandler::difference(TimeHandler larger) {

	return std::chrono::duration_cast<std::chrono::milliseconds>
		(larger.getTimeObject().time_since_epoch() - time.time_since_epoch()).count();
}

//used for precision
std::chrono::high_resolution_clock::time_point TimeHandler::getTimeObject() {
	return time;
}

//used for both
void TimeHandler::updateTime() {
	time = std::chrono::high_resolution_clock::now();
	system = std::chrono::system_clock::now();
}

long TimeHandler::getSecondsOfDay() {
	return (this->getSeconds() % day);
}

bool TimeHandler::openMarket() {
	long secondsOfDay = this->getSecondsOfDay();
	if (secondsOfDay >= OPEN && secondsOfDay <= CLOSE) {
		return true;
	}
	return false;
}

int TimeHandler::sleepTime() {
	int sleep = 0;
	if (OPEN > this->getSecondsOfDay()) {
		sleep = OPEN;
	}
	else {
		sleep = OPEN + day;
	}
	sleep = sleep - this->getSecondsOfDay();
	return sleep;
}

std::string TimeHandler::getDate() {
	auto in_time_t = std::chrono::system_clock::to_time_t(system);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d");
	return ss.str();
}